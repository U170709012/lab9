package shape.TwoDimesionalShape;

public class Triangle extends TwoDimensionalShape{

    private int a, b, c;

    public Triangle(int a, int b, int c, float height) {
        // base side = a
        super(a, height);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public float area() {
        return super.area() / 2;
    }

    @Override
    public float perimeter() {
        return this.a + this.b + this.c;
    }
}
