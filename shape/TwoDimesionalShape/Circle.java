package shape.TwoDimesionalShape;

public class Circle extends TwoDimensionalShape {

    public Circle(float radius) {
        super(radius, radius);
    }

    @Override
    public float area() {
        return (float) (Math.PI * (Math.pow(super.getWidth(), 2)));
    }

    @Override
    public float perimeter() {
        return (float) (2 * Math.PI * super.getWidth());
    }

}
