package shape.TwoDimesionalShape;

import shape.Shape;

public class TwoDimensionalShape extends Shape {

    protected TwoDimensionalShape(float width, float height) {
        super(width, height);

    }

    @Override
    public float area() {
        return super.getWidth() * super.getHeight();
    }

    @Override
    public float perimeter() {
        return 2 * (super.getWidth() + super.getHeight());
    }

}
