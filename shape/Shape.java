package shape;

public class Shape {
    private float width;
    private float height;
    private float length;

    protected Shape() {
        this.width = 1;
        this.height = 1;
        this.length = 1;

    }

    protected Shape(float width, float height) {
        this.width = width;
        this.height = height;
    }

    protected Shape(float width, float height, float length) {
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public float area() throws Exception {
        throw new Exception("Please implement a valid function.");
    }

    public float perimeter() throws Exception {
        throw new Exception("Please implement a valid function.");
    }

    public float volume() throws Exception {
        throw new Exception("Please implement a valid function.");
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

}
