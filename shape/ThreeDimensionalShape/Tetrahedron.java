package shape.ThreeDimensionalShape;

public class Tetrahedron extends ThreeDimensionalShape {
    public Tetrahedron(float side) {
        super(side, side, side);
    }

    @Override
    public float area() {
        return (float) (Math.sqrt(3) * Math.pow(super.getWidth(), 2));

    }

    @Override
    public float volume() {
        return (float) (Math.PI * Math.pow(super.getWidth(), 3) / 6 * Math.sqrt(2));

    }
}
