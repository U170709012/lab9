package shape.ThreeDimensionalShape;

public class Sphere extends ThreeDimensionalShape {

    public Sphere(float radius) {
        super(radius, radius, radius);
    }

    @Override
    public float area() {
        return (float) (4 * Math.PI * Math.pow(super.getWidth(), 2));

    }

    @Override
    public float volume() {
        return (float) (4 / 3 * Math.PI * Math.pow(super.getWidth(), 3));

    }

}