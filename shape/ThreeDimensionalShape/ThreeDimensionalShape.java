package shape.ThreeDimensionalShape;

import shape.Shape;

public class ThreeDimensionalShape extends Shape {

    protected ThreeDimensionalShape(float width, float height, float length) {
        super(width, height, length);
    }

    @Override
    public float area() {
        return 6 * super.getWidth() * super.getHeight();
    }

    @Override
    public float volume() {
        return super.getWidth() * super.getHeight() * super.getLength();
    }

}