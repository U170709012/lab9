package shape.ThreeDimensionalShape;

public class Cube extends ThreeDimensionalShape {

    public Cube(float side) {
        super(side, side, side);
    }

}