
import shape.Shape;
import shape.ThreeDimensionalShape.Cube;
import shape.ThreeDimensionalShape.Sphere;
import shape.ThreeDimensionalShape.Tetrahedron;
import shape.TwoDimesionalShape.Circle;
import shape.TwoDimesionalShape.Square;
import shape.TwoDimesionalShape.Triangle;

import java.io.File; // Import the File class
import java.io.FileNotFoundException; // Import this class to handle errors
import java.util.ArrayList;
import java.util.Scanner; // Import the Scanner class to read text files

public class Tester {

    public static void main(String[] args) {
        ArrayList<Shape> shapes = new ArrayList<Shape>();

        try {
            File instFile = new File("instructor.txt");
            Scanner myReader = new Scanner(instFile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] chars = data.split(" ");
                if (chars[0].equals("O")) {
                    if (chars[1].equals("C")) {
                        Shape circle = new Circle(Float.parseFloat(chars[2]));
                        shapes.add(circle);
                    } else if (chars[1].equals("S")) {
                        Shape sqaure = new Square(Float.parseFloat(chars[2]));
                        shapes.add(sqaure);
                    } else if (chars[1].equals("T")) {
                        Shape triangle = new Triangle(Integer.parseInt(chars[2]), Integer.parseInt(chars[3]),
                                Integer.parseInt(chars[4]), Float.parseFloat(chars[5]));
                        shapes.add(triangle);
                    } else if (chars[1].equals("SP")) {
                        Shape sphere = new Sphere(Float.parseFloat(chars[2]));
                        shapes.add(sphere);
                    } else if (chars[1].equals("CU")) {
                        Shape cube = new Cube(Float.parseFloat(chars[2]));
                        shapes.add(cube);
                    } else if (chars[1].equals("TE")) {
                        Shape tetrahedron = new Tetrahedron(Float.parseFloat(chars[2]));
                        shapes.add(tetrahedron);
                    }

                } else if (chars[0].equals("TA")) {
                    float total = 0;
                    for (int i = 0; i < shapes.size(); i++) {
                        try {
                            total = total + shapes.get(i).area();
                        } catch (Exception e) {

                        }

                    }

                    System.out.println("Total area: " + total);

                } else if (chars[0].equals("TP")) {
                    float total = 0;
                    for (int i = 0; i < shapes.size(); i++) {
                        try {
                            total = total + shapes.get(i).perimeter();
                        } catch (Exception e) {

                        }

                    }

                    System.out.println("Total perimeter: " + total);

                } else if (chars[0].equals("TV")) {
                    float total = 0;
                    for (int i = 0; i < shapes.size(); i++) {
                        try {
                            total = total + shapes.get(i).volume();
                        } catch (Exception e) {

                        }

                    }

                    System.out.println("Total volume: " + total);

                }

            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
}
